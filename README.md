# TP 2 - Machine virtuelle, réseau, serveurs, routage simple.
  
   | Name   | IP        | MAC               | Fonction                |
   |--------|-----------|-------------------|-------------------------|
   | lo     | 127.0.0.1 | 00:00:00:00:00:00 | Carte de loopback       |
   | enp0s8 | 10.0.2.15 | 08:00:27:b6:89:ab  | Carte réseau NAT       |
   
   IP base : 10.2.1.4
   ```bash
   # ip a
   1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
         valid_lft forever preferred_lft forever
      inet6 ::1/128 scope host
         valid_lft forever preferred_lft forever
   2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:b6:89:ab brd ff:ff:ff:ff:ff:ff
      inet 10.2.1.4/24 brd 10.2.1.255 scope global noprefixroute enp0s8
         valid_lft forever preferred_lft forever
      inet6 fe80::c87c:9447:9db2:38f9/64 scope link noprefixroute
         valid_lft forever preferred_lft forever
   ```
                                                                                                                                        
   `cd /etc/sysconfig/network-scripts` to change the ip adress
   
   `service network restart` pour restart le service réseau pour ensuite l'ip soit changed.

   3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
      link/ether 08:00:27:b6:89:ab brd ff:ff:ff:ff:ff:ff
      inet 10.2.1.5/24 brd 10.2.1.255 scope global noprefixroute enp0s8
         valid_lft forever preferred_lft forever
      inet6 fe80::c87c:9447:9db2:38f9/64 scope link noprefixroute
         valid_lft forever preferred_lft forever
   ```
   ping
   ```powershell
   PS C:\Windows\system32> ping 10.2.1.5
   
   Envoi d’une requête 'Ping'  10.2.1.5 :
   Réponse de 10.2.1.5 : octets=32 temps<1ms TTL=64
   Réponse de 10.2.1.5 : octets=32 temps=1 ms TTL=64
   Réponse de 10.2.1.5 : octets=32 temps<1ms TTL=64
   Réponse de 10.2.1.5 : octets=32 temps<1ms TTL=64
   
   ### 5. Appréhension de quelques commandes
   [root@localhost ~]#  nmap -A 10.2.1.5
   
   Starting Nmap 6.40 ( http://nmap.org ) at 2020-02-13 15:40 CET
   Nmap scan report for 10.2.1.5
   Host is up (0.00015s latency).
   Not shown: 999 closed ports
   PORT   STATE SERVICE VERSION
   22/tcp open  ssh     OpenSSH 7.4 (protocol 2.0)
   | ssh-hostkey: 2048 d6:21:5e:21:1e:ac:14:bd:0f:b7:2e:0a:1e:c8:8c:a9 (RSA)
   |_256 8b:a3:1d:26:60:10:ab:e1:6e:9b:2e:bb:67:e7:66:43 (ECDSA)
   Device type: general purpose
   Running: Linux 3.X
   OS CPE: cpe:/o:linux:linux_kernel:3
   OS details: Linux 3.7 - 3.9
   Network Distance: 0 hops
   
   On scanne les ports TCP/UDP
   ```powershell
   [root@localhost ~]# ss -ltunp
   Netid  State      Recv-Q Send-Q                              
   udp    UNCONN     0      0                                                *:68                                                           *:*                   users:(("dhclient",pid=3193,fd=6))
   udp    UNCONN     0      0                                        127.0.0.1:323                                                          *:*                   users:(("chronyd",pid=795,fd=5))
   udp    UNCONN     0      0                                            [::1]:323                                                       [::]:*                   users:(("chronyd",pid=795,fd=6))
   tcp    LISTEN     0      100                                      127.0.0.1:25                                                           *:*                   users:(("master",pid=1540,fd=13))
   tcp    LISTEN     0      128                                              *:22                                                           *:*                   users:(("sshd",pid=1215,fd=3))
   tcp    LISTEN     0      100                                          [::1]:25                                                        [::]:*                   users:(("master",pid=1540,fd=14))
   tcp    LISTEN     0      128                                           [::]:22                                                        [::]:*                   users:(("sshd",pid=1215,fd=4))
   ```
   ### 1. SSH
  `sshd` sur le port 22.
   
   On utilise la commande `ss -l -t`
   ```powershell
   [root@localhost ~]# ss -l -t
   State      Recv-Q Send-Q                                Local Address:Port                                                 Peer Address:Port
   LISTEN     0      100                                       127.0.0.1:smtp                                                            *:*
   LISTEN     0      128                                               *:ssh                                                             *:*
   LISTEN     0      100                                           [::1]:smtp                                                         [::]:*
   LISTEN     0      128                                            [::]:ssh                                                          [::]:*
   
   ```
   Pour modifier le port du service SSH: `/etc/ssh` et modifier le file `sshd_config`
   
   ### B. Netcat
   
   On fait 2 powershells sur le PC.
   connexion de la VM `nc -L -p 1025` et sur la VM. 
    `nc 192.168.0.20 1025` pour qu'ils puissent communiquer.
   Sur le second powershell, on utilise netstat.
   ```powershell
   PS C:\Windows\System32\WindowsPowerShell\v1.0> netstat

   Proto  Adresse locale         Adresse distante       État
   TCP    10.2.1.1:19022         10.2.1.5:ssh           ESTABLISHED
   [...]
   

